# EzPlatformDefaultCustomTagConfigs

Typically when setting up a new custom tag, `my_custom_tag` for example, you must set these configs:

```yaml
ezrichtext:
  custom_tags:
    my_custom_tag:
      template: '@ezdesign/custom_tag/my_custom_tag.html.twig'
      icon: '/assets/build/shared/images/custom-tag-icons.svg#my_custom_tag'
      is_inline: false
      attributes: {  }

ezplatform:
  system:
    admin_group:
      fieldtypes:
        ezrichtext:
          custom_tags:
            - my_custom_tag
          toolbars:
            my_custom_tag:
              buttons:
                ezmoveup:
                  priority: 80
                ezmovedown:
                  priority: 70
                ezcustomtagedit:
                  priority: 60
                ezanchor:
                  priority: 50
                ezembedleft:
                  priority: 40
                ezembedcenter:
                  priority: 30
                ezembedright:
                  priority: 20
                ezblockremove:
                  priority: 10
            ezadd:
              buttons:
                my_custom_tag:
                  priority: 1
                  visible: true
```

This is a lot of configuration for each custom tag, for what should be default behavior.

This bundle solves that problem!

It automatically adds `toolbars` and `ezadd` config that is the same for each custom tag. 
So adding a new custom tag becomes a matter of **only** adding this config:
```yaml
ezrichtext:
  custom_tags:
    my_custom_tag:
      template: '@ezdesign/custom_tag/my_custom_tag.html.twig'
      icon: '/assets/build/shared/images/custom-tag-icons.svg#my_custom_tag'
      is_inline: false
      attributes: {  }

ezplatform:
  system:
    admin_group:
      fieldtypes:
        ezrichtext:
          custom_tags:
            - my_custom_tag
```

## Installation

1. Require via composer
    ```bash
    composer require contextualcode/ezplatform-default-custom-tag-configs
    ```

2. Phew!